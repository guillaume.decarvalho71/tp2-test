package com.epsi.test.indutrialisation.tpRestaurant.model.commande;

public class CommandeBoisson implements ICommande {

    int montantCommande;
    int idServeur;

    public void setMontantCommande(int montantCommande) {
        this.montantCommande = montantCommande;
    }
    public int getMontantCommande() {
        return montantCommande;
    }
}
