/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.test.indutrialisation.tpRestaurant.model.Menu;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pollo
 */
public class MenuRestaurant implements MenuC {

    private MenuFranchise _parent;
    private Map<Plat, Double> plats = new HashMap<>();

    public MenuRestaurant(MenuFranchise _parent) {
        this._parent = _parent;
    }

    public Map<Plat, Double> getPlats() {
        return plats;
    }

    public void definirPlat(Plat plat, Double prix) {
        Double prixParent = _parent.recupererPrixPlat(plat);
        if (this._parent.getPlats().containsKey(plat)) {
            this.plats.put(plat, prixParent);
        } else {
            this.plats.put(plat, prix);
        }
    }

    @Override
    public Double recupererPrixPlat(Plat plat) {
        Double prixParent = _parent.recupererPrixPlat(plat);
        if (prixParent != null) {
            return prixParent;
        } else {
            return plats.get(plat);
        }
    }

}
