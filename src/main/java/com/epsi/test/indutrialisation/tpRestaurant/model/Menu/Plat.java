/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.test.indutrialisation.tpRestaurant.model.Menu;

/**
 *
 * @author pollo
 */
public class Plat {

    private String nomPlat;

    public Plat() {
    }

    public String getNomPlat() {
        return nomPlat;
    }

    public void setNomPlat(String nomPlat) {
        this.nomPlat = nomPlat;
    }

    public Plat(String nomPlat) {
        this.nomPlat = nomPlat;
    }

}
