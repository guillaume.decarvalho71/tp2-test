package com.epsi.test.indutrialisation.tpRestaurant.model;

import java.util.*;

public interface Responsable {

    List<Table> tables = new ArrayList<>();

    default void affecterTable(Table table){
        tables.add(table);
    }
}
