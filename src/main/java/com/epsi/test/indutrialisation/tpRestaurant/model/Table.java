package com.epsi.test.indutrialisation.tpRestaurant.model;

public class Table {
    private boolean estLibre=true, estAffectable=true;
    private Responsable responsableTable;

    public void affecterClient(Client client) {
        estLibre=false;
    }

    public void liberer() {
        this.estLibre = true;
        estAffectable = true;
        responsableTable = null;
    }

    public boolean isEstLibre() {
        return estLibre;
    }

    public boolean affecterResponsable(Responsable responsable){
        if (estAffectable = true){
            this.responsableTable = responsable;
            responsable.affecterTable(this);
            estAffectable = false;
            return false;
        }else{
            return true;
        }
    }



    public Responsable estAffectee(){
        if (responsableTable != null){
            return responsableTable;
        }else{
            return null;
        }
    }

}
