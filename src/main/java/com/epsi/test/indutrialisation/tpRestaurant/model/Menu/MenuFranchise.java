/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.test.indutrialisation.tpRestaurant.model.Menu;

import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author pollo
 */
public class MenuFranchise implements MenuC {

    private Map<Plat, Double> plats = new HashMap<>();

    public Map<Plat, Double> getPlats() {
        return plats;
    }

    public void definirPlat(Plat plat, Double prix) {
        this.plats.put(plat, prix);
    }

    public Double changerPrixPlat(Plat plat, Double prixPlat) {
        for (int i = 0; i < this.plats.size();) {
            this.plats.put(plat, prixPlat);
            return this.plats.get(plat);
        }
        return prixPlat;
    }

    @Override
    public Double recupererPrixPlat(Plat plat) {
        return plats.get(plat);
    }

}
