package com.epsi.test.indutrialisation.tpRestaurant.model.commande;

public class CommandeNourriture implements ICommande {

    int montantCommande;
    int idServeur;

    public void setMontantCommande(int montantCommande) {
        this.montantCommande = montantCommande;
    }
    public int getMontantCommande() {
        return montantCommande;
    }
}
