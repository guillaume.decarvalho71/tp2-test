package com.epsi.test.indutrialisation.tpRestaurant.model;


import com.epsi.test.indutrialisation.tpRestaurant.model.commande.CommandeBoisson;
import com.epsi.test.indutrialisation.tpRestaurant.model.commande.CommandeNourriture;
import com.epsi.test.indutrialisation.tpRestaurant.model.commande.ICommande;

import java.util.ArrayList;
import java.util.List;

public class Serveur implements Responsable{
    private int chiffreDAffaires;
    private int idServeur;
    List<ICommande> CommandePrises= new ArrayList<>() ;
    public Serveur() {
    }

    public void PriseCommande(CommandeNourriture commande) {
        this.CommandePrises.add(commande);
        this.chiffreDAffaires += commande.getMontantCommande();
    }
    public void PriseCommande(CommandeBoisson commande) {
        this.chiffreDAffaires += commande.getMontantCommande();
    }
    public int getChiffreDAffaires() {
        return chiffreDAffaires;
    }

    public void setChiffreDAffaires(int chiffreDAffaires) {
        this.chiffreDAffaires = chiffreDAffaires;
    }



}
