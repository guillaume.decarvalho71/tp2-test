package com.epsi.test.indutrialisation.tpRestaurant.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BooksController {

    // Get all books
    @GetMapping("/books")
    public List<String> getBooks() {
        List<String> list = new ArrayList<>();
        list.add("t123");
        list.add("t456");

        return list;
    }
}
