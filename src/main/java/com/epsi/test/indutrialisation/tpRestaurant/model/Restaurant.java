package com.epsi.test.indutrialisation.tpRestaurant.model;

import com.epsi.test.indutrialisation.tpRestaurant.model.Menu.MenuRestaurant;
import com.epsi.test.indutrialisation.tpRestaurant.model.commande.ICommande;

import com.epsi.test.indutrialisation.tpRestaurant.model.Menu.MenuRestaurant;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    private  Table[] table;
    private MaitreHotel maitreHotel;
    private Serveur[] responsable;
    private boolean estFiliale;
    private MenuRestaurant menu;
    private Franchise franchise;
    private int idRestaurant;


    public Restaurant(Table[] table, MaitreHotel maitreHotel, Serveur[] responsable) {
        this.table = table;
        this.maitreHotel = maitreHotel;
        this.responsable = responsable;

    }




    public void startService() {
        for (Table t: table) {
            if(t.estAffectee() == null){
                t.affecterResponsable(maitreHotel);
            }
        }
    }

    public List<Table> getListTable() {
        List<Table> tables=new ArrayList<>();
        for (Table t: table) {
            if (t.isEstLibre()){
                tables.add(t);
            }
        }
        return tables;
    }
    public List<ICommande> GetListeCommande(){
        ArrayList<ICommande> commandesRealise=new ArrayList<>();
        for (Serveur s:responsable) {
            commandesRealise.addAll( s.CommandePrises);
        }
        return commandesRealise;
    }

    public void appartenirFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public boolean avoirTableAffectee(){
        for (Table t: table) {
            if (t.estAffectee() != null){
                return true;
            }
        }
        return false;
    }

    public void stopService(){

    }

    public void affecterTablesFinService(Responsable responsable){
        for (Table t: table) {
            if (t.estAffectee() == null){
                t.affecterResponsable(responsable);
            }
        }
    }
    public void setEstFiliale(boolean estFiliale) {
        this.estFiliale = estFiliale;
    }

    public void definirMenu(MenuRestaurant menu) {
        this.menu = menu;
    }

    public MenuRestaurant getMenu() {
        return menu;
    }
}
