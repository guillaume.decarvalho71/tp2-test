package com.epsi.test.indutrialisation.tpRestaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpRestaurantApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpRestaurantApplication.class, args);
	}

}
