package com.epsi.test.indutrialisation.tpRestaurant.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DebutServiceTests {
    @DisplayName(  "ÉTANT DONNE un restaurant ayant 3 tables "+
            " QUAND le service commence "+
            " ALORS elles sont toutes affectées au Maître d'Hôtel")
    @Test
    public void affectation_table_mh(){
        //	ÉTANT DONNE un restaurant ayant 3 tables
        Table table1=new Table();
        Table table2=new Table();
        Table table3=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur[] responsables = new Serveur[]{};
        Table[] tables = new Table[]{table1,table2,table3};
        Restaurant restaurant=new Restaurant(tables, maitreHotel, responsables);

        //  QUAND le service commence
        restaurant.startService();

        //  ALORS elles sont toutes affectées au Maître d'Hôtel
        boolean test = true;
        for (Table t: tables) {
            if(t.estAffectee() != maitreHotel){
                test = false;
            }
        }
        assertTrue(test);
    }

    @DisplayName(  "ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur " +
            "QUAND le service débute " +
            "ALORS il n'est pas possible de modifier le serveur affecté à la table ")
    @Test
    public void affectation_table_mh_serveur(){
        //	ÉTANT DONNE un restaurant ayant 3 dont une affectée à un serveur
        Table table1=new Table();
        Table table2=new Table();
        Table table3=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table1,table2,table3};
        Restaurant restaurant=new Restaurant(tables, maitreHotel, responsables);

        //  QUAND le service débute
        table1.affecterResponsable(serveur);
        restaurant.startService();

        //  ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
        boolean test = true;
        if(table1.estAffectee() != serveur){
            test = false;
        }
        if(table2.estAffectee() != maitreHotel || table3.estAffectee() != maitreHotel){
            test = false;
        }
        assertTrue(test);
    }

    @DisplayName(  "ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur " +
            "QUAND le service débute " +
            "ALORS il n'est pas possible de modifier le serveur affecté à la table ")
    @Test
    public void affectation_table_mh_serveur_lock(){
        //	ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
        Table table1=new Table();
        Table table2=new Table();
        Table table3=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table1,table2,table3};
        Restaurant restaurant=new Restaurant(tables, maitreHotel, responsables);

        //  QUAND le service débute
        table1.affecterResponsable(serveur);
        restaurant.startService();

        //  ALORS il n'est pas possible de modifier le serveur affecté à la table
        boolean test = true;
        if(table1.affecterResponsable(serveur)){
            test = false;
        }
        assertTrue(test);
    }

    @DisplayName(  "ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur " +
            "ET ayant débuté son service" +
            "QUAND le service se termine " +
            "ET qu'une table est affectée à un serveur" +
            "ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel ")
    @Test
    public void affectation_table_mh_serveur_fin_service(){
        //	ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
        Table table1=new Table();
        Table table2=new Table();
        Table table3=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table1,table2,table3};
        Restaurant restaurant=new Restaurant(tables, maitreHotel, responsables);

        //  ET ayant débuté son service
        table1.affecterResponsable(serveur);
        restaurant.startService();

        // QUAND le service se termine

        restaurant.stopService();

        // ET qu'une table est affectée à un serveur
        if(restaurant.avoirTableAffectee()) {
            //  ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
            boolean test = true;
            if(table1.estAffectee() != serveur){
                test = false;
            }
            if(table2.estAffectee() != maitreHotel || table3.estAffectee() != maitreHotel){
                test = false;
            }
            assertTrue(test);
        }
    }

}
