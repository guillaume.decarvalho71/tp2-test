/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epsi.test.indutrialisation.tpRestaurant.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import com.epsi.test.indutrialisation.tpRestaurant.model.commande.CommandeNourriture;


/**
 *
 * @author pollo
 */
public class ServeurTest {

    /**
     * Test of getChiffreDAffaires method, of class Serveur.
     */
    @Test
    public void testGetChiffreDAffaires() {
        // ÉTANT DONNÉ un nouveau serveur
        Serveur serveur = new Serveur();
        //QUAND on récupére son chiffre d'affaires 
        int result = serveur.getChiffreDAffaires();
        //ALORS celui-ci est à 0
        assertEquals(result, 0);
    }

    @Test
    public void testGetChiffreDAffairesCommande() {
        //ÉTANT DONNÉ un nouveau serveur
        Serveur serveur = new Serveur();
        //QUAND il prend une commande
        CommandeNourriture commande = new CommandeNourriture();
        commande.setMontantCommande(100);
        serveur.PriseCommande(commande);

        //ALORS son chiffre d'affaires est le montant de celle-ci
        assertEquals(serveur.getChiffreDAffaires(), commande.getMontantCommande());
    }

    @Test
    public void getChiffreDAffairesSomme() {
        Serveur serveur = new Serveur();
        CommandeNourriture commande1 = new CommandeNourriture();
        commande1.setMontantCommande(100);
        //ÉTANT DONNÉ un serveur ayant déjà pris une commande
        serveur.PriseCommande(commande1);
        //QUAND il prend une nouvelle commande
        CommandeNourriture commande2 = new CommandeNourriture();
        commande2.setMontantCommande(400);
        serveur.PriseCommande(commande2);
        
        int sommeDeuxCommandes = commande1.getMontantCommande() + commande2.getMontantCommande();
        //ALORS son chiffre d'affaires est la somme des deux commandes
        System.out.println(sommeDeuxCommandes);
        assertEquals(serveur.getChiffreDAffaires(), sommeDeuxCommandes);
    }

}
