package com.epsi.test.indutrialisation.tpRestaurant.model;



import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;


import com.epsi.test.indutrialisation.tpRestaurant.model.commande.ICommande;
import com.epsi.test.indutrialisation.tpRestaurant.model.commande.CommandeBoisson;
import com.epsi.test.indutrialisation.tpRestaurant.model.commande.CommandeNourriture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;



class ScopeCommandeTest {

    @DisplayName("ÉTANT DONNE un serveur dans un restaurant" +
            "QUAND il prend une commande de nourriture" +
            "ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant")
    @Test
	void Test1() {
        //ÉTANT DONNE un serveur dans un restaurant
        Table table1=new Table();
        Table table2=new Table();
        Table table3=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table1,table2,table3};
        Restaurant restaurant=new Restaurant(tables, maitreHotel, responsables);
        CommandeNourriture commande = new CommandeNourriture();

	    //QUAND il prend une commande de nourriture
        serveur.PriseCommande(commande);
	    //ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant
        List<ICommande> listeCommande = restaurant.GetListeCommande();
        assertTrue(listeCommande.contains(commande));

	}
    @DisplayName("ÉTANT DONNE un serveur dans un restaurant" +
            "QUAND il prend une commande de boissons" +
            "ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant")
    @Test
    void Test2(){
        //ÉTANT DONNE un serveur dans un restaurant
        Table table1=new Table();
        Table table2=new Table();
        Table table3=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table1,table2,table3};
        Restaurant restaurant=new Restaurant(tables, maitreHotel, responsables);
        CommandeBoisson commande = new CommandeBoisson();
        //QUAND il prend une commande de boissons
        serveur.PriseCommande(commande);
        //ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant
        List<ICommande> listeCommande = restaurant.GetListeCommande();
        assertFalse(listeCommande.contains(commande));
    }

}
