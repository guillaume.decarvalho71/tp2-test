/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epsi.test.indutrialisation.tpRestaurant.model;
/**
 *
 * @author pollo
 */

import java.util.Map;

import com.epsi.test.indutrialisation.tpRestaurant.model.Menu.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    @Test
    public void testMenu() {
        //ÉTANT DONNE un restaurant ayant le statut de filiale d'une franchise
        Table table=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table};
        Restaurant restaurant = new Restaurant(tables,maitreHotel,responsables);
        restaurant.setEstFiliale(true);
        //ET une franchise définissant un menu ayant un plat
        Franchise franchise = new Franchise();

        MenuFranchise menuFranchise = new MenuFranchise();

        Plat plat = new Plat();
        plat.setNomPlat("Plat test");

        menuFranchise.definirPlat(plat, 400.00);
        franchise.definirMenu(menuFranchise);

        //QUAND la franchise modifie le prix du plat
        franchise.getMenu().changerPrixPlat(plat, 200.00);
        MenuRestaurant menuRestaurant = new MenuRestaurant(menuFranchise);
        menuRestaurant.definirPlat(plat, 400.00);
        restaurant.definirMenu(menuRestaurant);

        Map<Plat, Double> platsRestaurant = restaurant.getMenu().getPlats();
        Map<Plat, Double> platsFranchise = franchise.getMenu().getPlats();

        //ALORS le prix du plat dans le menu du restaurant est celui défini par la franchise  
        assertEquals(platsRestaurant.get(plat), platsFranchise.get(plat));

    }


    @Test
    public void testMenu2() {
        //ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
        Table table=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table};
        Restaurant restaurant = new Restaurant(tables,maitreHotel,responsables);
        Franchise franchise = new Franchise();
        restaurant.appartenirFranchise(franchise);

        MenuFranchise menuFranchise = new MenuFranchise();
        MenuRestaurant menuRestaurant = new MenuRestaurant(menuFranchise);

        Plat plat1 = new Plat("Plat 1");

        menuFranchise.definirPlat(plat1, 250.00);
        menuRestaurant.definirPlat(plat1, 250.00);

        //ET une franchise définissant un menu ayant le même plat
        franchise.definirMenu(menuFranchise);
        restaurant.definirMenu(menuRestaurant);

        //QUAND la franchise modifie le prix du plat        
        franchise.getMenu().changerPrixPlat(plat1, 850.00);

        //ALORS le prix du plat dans le menu du restaurant reste inchangé 
        Map<Plat, Double> platsFranchise = franchise.getMenu().getPlats();
        Map<Plat, Double> platsRestaurant = restaurant.getMenu().getPlats();

        Double prixFranchise = platsFranchise.get(plat1);
        Double prixRestaurant = platsRestaurant.get(plat1);

        assertNotEquals(prixFranchise, prixRestaurant);
    }

    @Test
    public void testMenu3() {
        //ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
        Table table=new Table();
        MaitreHotel maitreHotel = new MaitreHotel();
        Serveur serveur = new Serveur();
        Serveur[] responsables = new Serveur[]{serveur};
        Table[] tables = new Table[]{table};
        Restaurant restaurant = new Restaurant(tables,maitreHotel,responsables);
        Franchise franchise = new Franchise();
        restaurant.appartenirFranchise(franchise);
        
        MenuFranchise menuFranchise = new MenuFranchise();
        MenuRestaurant menuRestaurant = new MenuRestaurant(menuFranchise);
        
        Plat platRestaurant = new Plat("Plat 1");        
        menuRestaurant.definirPlat(platRestaurant, 800.00);
        menuFranchise.definirPlat(platRestaurant, 800.00);
        
        Plat platFranchise = new Plat("Plat 2");
        
        restaurant.definirMenu(menuRestaurant);
        franchise.definirMenu(menuFranchise);
        
        //QUAND la franchise ajoute un nouveau plat
        menuFranchise.definirPlat(platFranchise, 400.00);
        menuRestaurant.definirPlat(platFranchise, null);
        
        //ALORS la carte du restaurant propose le premier plat au prix du restaurant et le second au prix de la franchise        
        assertEquals(franchise.getMenu().recupererPrixPlat(platFranchise), restaurant.getMenu().recupererPrixPlat(platFranchise));
    }
}
