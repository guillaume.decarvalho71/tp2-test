#!/bin/bash

mkdir repo
cd ./repo
git init
git remote add repo https://gitlab.com/guillaume.decarvalho71/tp2-test.git
git fetch repo main
git checkout main

mvn clean
mvn install
cd target


test='["t123","t456"]'
java -jar *.jar >test  &
sleep 10
result="`wget -qO- 127.0.0.1:8080/api/books`"

search_terms='java' 
kill $(ps aux | grep "$search_terms" | grep -v 'grep' | awk '{print $2}')
echo $result
if [ "$test" == "$result" ]
then



    #deploy
    cp *.jar ../../executable.jar
fi
cd ../..
java -jar executable.jar --server.port=80 >log.txt &
chmod -R 0777 ./repo 
rm -r ./repo